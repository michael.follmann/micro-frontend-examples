# iFrame

This example uses client-side inclusion to display multiple web applications as one consistent UI.
The different domains of the portal are included with an iFrame. 
A gateway is used as single entry point to make the applications reachable on the same port (same-origin policy).

## Architecture
```mermaid
graph TD;
    overview-->blue;
    overview-->green;
    overview-->red;
```

## Start
```
docker-compose up
```

## Stop
You can either terminate it with `Ctrt+c` in the terminal you started it in or use:
```
docker-compose down
```
