# Micro Frontend Examples

This project contains several examples that demonstrate the usage of micro frontend architecture.
The goal is to implement the same simple application with different architecture styles.

## Demo Application
The demo application is called _Colorful Portal_ and mmimics a portal with three distinguishable domains:
* **blue**
* **green**
* **red**

There is a shared navigation to enable users to seamlessly switch between these domains.
A header is displayed on each page and the relevant navigation links.
In addition, an overview page is used as entry point for the portal.
Footer

## Prerequisites

### Docker
A Docker installation is required to start these examples on your machine.
Each example has a Docker Compose file that orchestrates the required containers.

### Port 8783
Port 8783 is used to map and expose the application on your machine.
In case this port is already used:
* terminate the program that is using it
* or modify the `docker-compose.yml` files to use another available port
 
After starting an example, you should be able to test it in your browser:
http://localhost:8783/
  
## Implementations

* [Monolith](monolith/README.md)
* [Server Side Includes (SSI)](ssi/README.md)
* [iFrame](iframe/README.md)
