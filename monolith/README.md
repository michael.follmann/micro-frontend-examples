# Monolith

Since it consists only of one artifact, this example cannot be considered as micro frontend architecture.
It servers as the baseline for other implementations.
A single NGINX container is used to deploy it.

## Start
```
docker-compose up
```

## Stop
You can either terminate it with `Ctrt+c` in the terminal you started it in or use:
```
docker-compose down
```
