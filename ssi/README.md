## Server Side Includes (SSI) Example

This example applies the pattern of server-side inclusion and aggregates multiple independent UIs in the backend before returning the result to the client.
A gateway is used as single entry point to make the applications reachable on the same port (same-origin policy).
Header and footer are independent services that are included by:
* overview
* blue
* green

`red` does not use the central header and footer and has a local header/footer copy.

NGINX has module for [Server Side Includes](https://en.wikipedia.org/wiki/Server_Side_Includes) that can be activated with configuration.

## Architecture
```mermaid
graph TD;
    overview-->header;
    overview-->footer;
    blue-->header;
    blue-->footer;
    green-->header;
    green-->footer;
    red;
```

## Start
```
docker-compose up
```

## Stop
You can either terminate it with `Ctrt+c` in the terminal you started it in or use:
```
docker-compose down
```
